package screens.com.example.userlocation;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.location.Address;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements LocationListener{

    protected LocationListener locationListener;
    protected LocationManager locationManager;
    protected Context context;
    private TextView textView;
    String lat, provider;
    private String latitude, longitude;
    protected boolean gps_enabled, network_enabled;
    public static final int LOCATION_PERMISSION_REQUEST_CODE = 99;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.text_view);

        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (Build.VERSION.SDK_INT >= 23) { // Marshmallow

                ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        LOCATION_PERMISSION_REQUEST_CODE);
            }

        } else {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {

            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                try {
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
                } catch (SecurityException e) {
                    e.printStackTrace();
                }
            } else { // if permission is not granted

                // decide what you want to do if you don't get permissions
            }
        }
    }

    @Override
    public void onLocationChanged(Location location){
        textView = findViewById(R.id.text_view);
        textView.setText("Latitude:"  + location.getLatitude() + ", Longitude: "+ location.getLongitude());
    }

    @Override
    public void onProviderDisabled(String provider){
        Log.d("Latitude", "disabled");
    }

    @Override
    public void onProviderEnabled(String provider){
        Log.d("Latitude", "enabled");
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras){
        Log.d("Latitude", "status");
    }
}
